import pandas as pd
import numpy as np

"""
Notes:
- When inserting questions into quiz the answers (right and wrong) needs to be shuffled
"""

#Input matrix (n difficulty by m category)
input=pd.read_csv('input_matrix_nbymNo.csv', header=None)
print(input)

OUTPUT_NAME='Questions_NoPython'

#category and difficulty name to integer
difficulty_levels=pd.read_csv('difficulty_levels.csv')
category_names=pd.read_csv('category_names.csv')

data=pd.read_excel('Questions_Bank.xlsx', sheet_name='Questions')
# data['Difficulty']=data['Difficulty'].astype(str)
# data['Category']=data['Category'].astype(str)

length=len(data.index)

questions_selected=pd.DataFrame()

def selecting_questions(questions, num_qs):
    """
    This function selects the needed amount of questions from the
    given list of questions

    Parameters
    ----------
    questions: dataframe
        A pandas dataframe with a list of questions to select from

    num_qs: integer
        The number of questions to select

    Returns
    -------
    A pandas dataframe with the selected questions
    """
    #Shuffle the questions list
    questions=questions.sample(frac=1).reset_index(drop=True)
    #Check for duplicates of specific question (Question Number appearing more than once)
    questions.drop_duplicates(subset='Q Number', keep='first', inplace=True)
    #If the list of questions is smaller than number of questions to select,
    #decrease the amount of questions to select to select the whole list
    if num_qs > len(questions.index):
        num_qs=len(questions.index)

    return questions[:num_qs]

"""
Go through each difficulty and category combination and select the specified
amount of questions from those difficulty and category combinations
"""
for d in range(len(difficulty_levels.index)):
    for c in range(len(category_names.index)):
        num_qs=input.iloc[d][c]
        #If no questions need to be selected from category, then skip
        if num_qs==0:
            continue
        #Get name of difficulty and category
        difficulty=list(difficulty_levels.iloc[d])[1]
        category=list(category_names.iloc[c])[1]
        #Select all questions from the specified category and difficulty
        temp=data.loc[(data['Difficulty']==str(difficulty)) & (data['Category']==str(category))]
        #Get a selection of questions
        selection=selecting_questions(temp, num_qs)
        questions_selected=questions_selected.append(selection)
        del temp

#Output selected questions to a csv
print(questions_selected.head(5))
questions_selected.to_csv(OUTPUT_NAME+'.csv', index=False, header=None)
