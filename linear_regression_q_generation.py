#Inus Grobler
#April 2020

import random
import pandas as pd
import numpy as np
from numpy import array
from sklearn.linear_model import LinearRegression

class LinearRegressionQuestion():
"""
Generate a multiple choice question randomly of the form:

    Question: Given the following data pairs (x, y), find the linear regression equation.
    (1, 1.24), (2, 5.23), (3, 7.24), (4, 7.60), (5, 9.97), (6, 14.31), (7, 13.99), (8, 14.88), (9, 18.04), (10, 20.70)

    Answers:    A) y = 1.979 x + 0.436
                B) y = 0.49 x
                C) y = 0.490 x - 0.053
                D) y = 2.04 x

    Parameters
    ----------
    min_x: integer
            the minimum value of the x values that are generated
    max_x: integer
            the maximum value of the x values that are generated
    min_y: integer
            the minimum value of the y values that are generated
    max_y: integer
            the maximum value of the y values that are generated
    size: integer
            the amount of numbers to generate of x and y values in calculating the linear regression formula
    deci_x: integer
            the number of decimal places the x_values must have
    deci_y: integer
            the number of decimal places the y_values must have
"""
    def __init__ (self,min_x=-100,max_x=100,min_y=-100,max_y=100, size=10, deci_x=0, deci_y=2):
        self.MIN_X=min_x
        self.MAX_X=max_x
        self.MIN_Y=min_y
        self.MAX_Y=max_y
        self.SIZE=size
        self.DECI_X=deci_x
        self.DECI_Y=deci_y

    def generate_numbers(self, start, end, size):
        """
        This funtion generates a list of random numbers with the specified
        start value, end value and the amount of numbers to generate.

        Parameters
        ----------
        start: interger
            The lowest value of the random numbers to be generated

        end: interger
            The highest value of the random numbers to be generated

        size: integer
            The amount of numbers to generate

        Returns
        -------
        A list of random numbers are returned
        """
        rand_num=[]
        for k in range(size):
            rand_num.append(random.randint(start,end))
        return rand_num

    def linear_regression_equation(self, x_values, y_values):
        """
        This funtion determines the linear regression equation
        based on the given x and y values

        Parameters
        ----------
        x_values: list of floats/integers
                The x values of the linear regression
        y_values: list of floats/integers
                The y values of the linear regression

        Returns
        -------
        A string of the linear regression equation of the given x and y values
        """
        x=(array(x_values).reshape(-1,1)).copy()
        y=y_values.copy()

        regressor=LinearRegression()
        regressor.fit(x,y)
        #To retrieve the intercept:
        intercept=round(regressor.intercept_,3)
        #For retrieving the slope:
        slope=round(regressor.coef_[0],3)
        if np.sign(intercept)==1:
            sign_intercept='+'
        else:
            sign_intercept='-'
        return f"y = {slope}x {sign_intercept} {abs(intercept)}"

    def generate_question(self):
        """
        This funtion generates a multiple choice question with 4 possible choices.
        Type of question:

        Parameters
        ----------
        no parameters

        Returns
        -------
        A tuple with the first index being the question and
        the second index being a list of the 4 choices
        The first of the 4 choices is the answer
        """
        x_factor=10**self.DECI_X
        y_factor=10**self.DECI_Y
        self.list_x_values=self.generate_numbers(self.MIN_X*x_factor,self.MAX_X*x_factor,self.SIZE)
        if x_factor>1:
            self.list_x_values[:]=[x/x_factor for x in self.list_x_values]
        self.list_y_values=self.generate_numbers(self.MIN_Y*y_factor,self.MAX_Y*y_factor,self.SIZE)
        if y_factor>1:
            self.list_y_values[:]=[y/y_factor for y in self.list_y_values]

        # Question text
        question=f"Given the following data pairs (x, y), "
        question+=f"find the linear regression equation.\n"

        for x,y in zip(self.list_x_values,self.list_y_values):
            question+=f"({x}, {y}), "

        #Generate 4 choices for the MCQ
        choices=self.create_choices()
        return(question, choices)

    def create_choices(self):
        """
        This function generates the different choices for the multiple choice

        answer: the answer of the question
        wrong_answer1: a wrong answer using a shuffled list of x_values
        wrong_answer2: a wrong answer using a shuffled list of y_values
        wrong_answer3: a wrong answer using x_values with an opposite sign
                        and y_values halved

        Returns
        -------
        A tuple with the 4 possible choices in the multiple choice
        """
        answer=self.linear_regression_equation(self.list_x_values,self.list_y_values)

        x_values_shuffled=random.sample(self.list_x_values, len(self.list_x_values))
        wrong_answer1=self.linear_regression_equation(x_values_shuffled,self.list_y_values)

        y_values_shuffled=random.sample(self.list_y_values, len(self.list_y_values))
        wrong_answer2=self.linear_regression_equation(self.list_x_values,y_values_shuffled)

        x_values_changed=[x*-1 for x in self.list_x_values]
        y_values_changed=[y/2 for y in self.list_y_values]
        wrong_answer3=self.linear_regression_equation(x_values_changed,y_values_changed)

        return (answer, wrong_answer1, wrong_answer2, wrong_answer3)

linear_question=LinearRegressionQuestion()
question_test=linear_question.generate_question() #Tuple with question and choices
                                            #First choice is answer other 3 are wrong answers
print(question_test)

NUM_Q_CREATE=10 #number of questions to generate

list_q_s=[]

for n in range(NUM_Q_CREATE):
    question=linear_question.generate_question()
    q_breakdown=[]
    q_breakdown.append(question[0])
    q_breakdown.append(question[1][0])
    q_breakdown.append(question[1][1])
    q_breakdown.append(question[1][2])
    q_breakdown.append(question[1][3])
    list_q_s.append(q_breakdown)

# Output the questions to csv
df=pd.DataFrame(list_q_s,columns=['question','answer','wrong_answer1','wrong_answer2', 'wrong_answer3'])
df.to_csv('10_questions_linear_regression.csv')
