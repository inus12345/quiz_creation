#Inus Grobler
#May 2020

import random
import pandas as pd
import numpy as np
"""
Question: If PALE is coded as 2134, EARTH is coded as 41590, how can is PEARL be coded in that language?
"""

class CodingDecoding():
    """
    The class object is used to create a multiple choice question of the form:
        Question: If PALE is coded as 2134, EARTH is coded as 41590, how can is PEARL be coded in that language?
    """
    def __init__ (self):
        self.list_numbers=[0,1,2,3,4,5,6,7,8,9]
        self.ENGLISH_WORDS=pd.read_csv('words_alpha.txt')
        #Remove words 2 characters or shorter or 11 characters and longer to improve speed
        self.ENGLISH_WORDS.a=self.ENGLISH_WORDS.a.apply(lambda x: x if (len(str(x)) > 2  and len(str(x)) < 11) else np.nan)
        self.ENGLISH_WORDS=self.ENGLISH_WORDS.dropna()

    def generate_numbers(self, start, end, size):
        """
        This funtion generates a list of random numbers with the specified
        start value, end value and the amount of numbers to generate.

        Parameters
        ----------
        start: interger
            The lowest value of the random numbers to be generated

        end: interger
            The highest value of the random numbers to be generated

        size: integer
            The amount of numbers to generate

        Returns
        -------
        A list of random numbers are returned
        """
        rand_num=[]
        for k in range(size):
            rand_num.append(random.randint(start,end))
        return rand_num

    def select_random_word(self, len_word):
        """
        This funtion randomly selects words from a english word list.
        The word selected must be len_word long
        and either no repeating characters or one character repeating

        If these conditions are not met a new random selection is made
        until these conditions placeholder

        Parameters
        ----------
        len_word: interger
            The length of the word that needs to be selected

        Returns
        -------
        A random word that satisfies the criteria of:
        - len_word characters in the word
        - no repeating characters or one character repeating
        """
        word_list_len=len(self.ENGLISH_WORDS.index) #length of the enlish words list
        selection=random.randint(0,word_list_len) #select random number
        not_valid=True #This boolean is true until a valid word is found

        while not_valid: #repeat until a valid word is found
            try:
                word=str(self.ENGLISH_WORDS.iloc[selection][0])
            except:
                word=''
                print('Error retreiving word. Will try again.')

            if len(word)==len_word and len(set(word))>=(len(word)-1):
                not_valid=False #make false because a valid word has been found
            else: #repeat select random number to try another word
                selection=random.randint(0,word_list_len)

        return word.upper() #return the word in all uppercase

    def generate_matching_words(self):
        """
        - This funtion randomly selects three words from a english word list.
        - The first and second word is part of the question and the final word
            is the word that is used in getting the answer.
        - The first word is between 3 and 6 characters (random)
        - The second word is 10 minus the first words length long
        - The final word is between 5 and 10 words long
        - The final word's characters must all be in word1 and word2
            otherwise new words need to be selected until there is a match

        Returns
        -------
        A tuple of three words is returned.
        The three words are used in the question
        """
        retry=True #retry is true until the correct words are selected
        while retry:
            # The first word is of length between 3 and 6 characters long
            first_word_length=random.randint(3,6)
            # The first and second word together must be 10 characters long
            second_word_length=10-first_word_length
            word1=self.select_random_word(first_word_length)
            word2=self.select_random_word(second_word_length)
            # The 3rd word is the length of the character sets of the 2 words
            # minus a random value between 0 and 3
            third_word_length=len(set(word1))+len(set(word2))-random.randint(0,3)
            word3=self.select_random_word(third_word_length)

            character_set=list(set(word1+word2)) #set of characters used in the 2 words
            retry=False
            for ch in list(set(word3)):
                if ch not in character_set:
                    retry=True #change retry to true if there is a character not in the character set
        return (word1, word2, word3)

    def encoding_word(self, word):
        """
        This function encodes the word to numbers based on the letter_to_number
        dictionary

        Returns
        -------
        A string with the encoding numbers of the word
        """
        word_encode=''
        for ch in word:
            word_encode+=str(self.letter_to_number[ch])
        return word_encode

    def generate_question(self):
        """
        This function generates a question and the respective choices for possible answers.

        Returns
        -------
        A tuple with the question and a list of the 4 possible choices
        """
        #Generate three words to use in the question
        first_word, second_word, final_word=self.generate_matching_words()
        # randomly shuffle list_numbers
        random.shuffle(self.list_numbers)
        self.letter_to_number={} #letter to number is a dictionary to map letters to numbers
        character_set=list(set(first_word+second_word)) #the set of characters in the two words
        # create a letter_to_number mapping to encode the words
        for k,v in zip(character_set,range(len(character_set))):
            self.letter_to_number[k]=self.list_numbers[v]
        # encode the words of the question
        first_word_encoded=self.encoding_word(first_word)
        second_word_encoded=self.encoding_word(second_word)
        final_word_encoded=self.encoding_word(final_word)

        question=f"If {first_word} is coded as {first_word_encoded}, "\
                    f"{second_word} is coded as {second_word_encoded}, "\
                    f"how can is {final_word} be coded in that language?"

        choices=self.create_choices(first_word_encoded,second_word_encoded,final_word_encoded)

        return (question, choices) # return tuple of question text and the 4 choices


    def create_choices(self,first_encoded, second_encoded, ans):
        """
        This function generates the different choices for the multiple choice

        answer: the answer of the question
        wrong_answer1: a wrong answer with using numbers randomly from the first and second word
        wrong_answer2: a wrong answer with the answer shuffled
        wrong_answer3: a wrong answer with a fake word, look for a word until a match is found
                    i.e. a word that does not have a character not found in word 1 and word 2

        Returns
        -------
        A tuple with the 4 possible choices in the multiple choice
        """
        answer=ans #the answer to the question
        #wrong answer 1 using numbers randomly from the first and second word
        wrong_answer1=''
        same_as_answer=True
        while same_as_answer:
            for i in range(len(ans)):
                choose_randomly=random.randint(0,1) #choose randomly between 0 and 1
                if choose_randomly==0 and i<len(first_encoded):
                    wrong_answer1+=first_encoded[i]
                elif choose_randomly==1 and i<len(second_encoded):
                    wrong_answer1+=second_encoded[i]
                else:
                    wrong_answer1+=ans[i]
                if wrong_answer1!=ans:
                    same_as_answer=False
        #wrong answer 2 with the answer shuffled
        random_answer=random.sample(ans,len(ans))
        wrong_answer2=''
        for r in random_answer:
            wrong_answer2+=r
        #wrong answer 3 with a fake word, look for a word until a match is found
        #i.e. a word that does not have a character not found in word 1 and word 2
        wrong_answer3=''
        still_error=True
        while still_error:
            try:
                fake_word=self.select_random_word(len(ans))
                wrong_answer3=self.encoding_word(fake_word)
                still_error=False
            except:
                still_error=True
                continue
        #return a tuple of the 4 possible choices
        return (str(answer), str(wrong_answer1), str(wrong_answer2), str(wrong_answer3))


coding_decoding=CodingDecoding()
question_test=coding_decoding.generate_question() #Tuple with question and choices

print(question_test)
                                             #First choice is answer other 3 are wrong answers
NUM_Q_CREATE=10

list_q_s=[]

for n in range(NUM_Q_CREATE):
    question=coding_decoding.generate_question()
    q_breakdown=[]
    q_breakdown.append(question[0])
    q_breakdown.append(question[1][0])
    q_breakdown.append(question[1][1])
    q_breakdown.append(question[1][2])
    q_breakdown.append(question[1][3])
    list_q_s.append(q_breakdown)

df=pd.DataFrame(list_q_s,columns=['question','answer','wrong_answer1','wrong_answer2', 'wrong_answer3'])
df.to_csv('10_questions_coding_decoding.csv')
