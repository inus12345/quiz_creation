#Inus Grobler
#April 2020

import random
import pandas as pd

"""
Question: Two trains start from A and B respectively and
travel towards each other at a speed of 60 km/h and 75 km/h respectively.
By the time they meet, the second train has traveled 70 km more than the first.
The distance between A and B is:
"""

class TwoTrainsQuestion():
    def __init__ (self):
        self.max_speed=120
        self.min_speed=50

    def generate_numbers(self, start, end, size):
        """
        This funtion generates a list of random numbers with the specified
        start value, end value and the amount of numbers to generate.

        Parameters
        ----------
        start: interger
            The lowest value of the random numbers to be generated

        end: interger
            The highest value of the random numbers to be generated

        size: integer
            The amount of numbers to generate

        Returns
        -------
        A list of random numbers are returned
        """
        rand_num=[]
        for k in range(size):
            rand_num.append(random.randint(start,end))
        return rand_num

    def generate_question(self):
        """
        This funtion generates a multiple choice question with 4 possible choices.
        Type of question: Two trains start from A and B respectively and
        travel towards each other at a speed of 60 km/h and 75 km/h respectively.
        By the time they meet, the fastest train has traveled 70 km more than the first.
        The distance between A and B in kilometer is?

        Parameters
        ----------
        no parameters

        Returns
        -------
        A tuple with the first index being the question and
        the second index being a list of the 4 choices
        The first of the 4 choices is the answer
        """

        numbers_too_close=True
        while numbers_too_close:
            # Generate 3 random integer, the first being the speed of the first train,
            # the second being the speed of the second train and the third integer is the
            # distance between the trains
            self.q_variables=self.generate_numbers(self.min_speed,self.max_speed,3)
            #Make sure the generate numbers are 15 apart from each other to improve the question quality
            if (abs(self.q_variables[0]-self.q_variables[1])>=15
            and abs(max(self.q_variables[1],self.q_variables[0])-self.q_variables[2])>=15):
                numbers_too_close=False
        # Question text
        question=f"Two trains start from A and B respectively and travel "
        question+=f"towards each other at a speed of {self.q_variables[0]}km/h "
        question+=f"and {self.q_variables[1]}km/h respectively. By the time they "
        question+=f"meet, the fastest train has traveled {self.q_variables[2]}km more "
        question+="than the first. The distance between A and B in kilometer is?"

        #Generate 4 choices for the MCQ
        choices=self.create_choices()

        return (question, choices)

    def create_choices(self):
        """
        This function generates the different choices for the multiple choice

        answer: the answer of the question
        wrong_answer1: a wrong answer using hours_traveled*average_speed
        wrong_answer2: a wrong answer using the speed of one of the trains chose randomly
                        divided by the hours the trains travelled
        wrong_answer3: a wrong answer using hours_traveled*difference_speed

        Returns
        -------
        A tuple with the 4 possible choices in the multiple choice
        """
        #sum of the trains speeds
        sum_speed=self.q_variables[0]+self.q_variables[1]
        #difference between the two trains speeds
        difference_speed=abs(self.q_variables[0]-self.q_variables[1])
        #Hours travelled of the trains
        hours_traveled=self.q_variables[2]/difference_speed
        #The average speed of the trains
        average_speed=sum_speed/2
        #The answer to the MCQ
        answer=round(hours_traveled*sum_speed,2)

        #The other 3 wrong answers
        wrong_answer1=round(hours_traveled*average_speed,2)
        choose_train=self.generate_numbers(0,1,1)[0] #Choose randomly a train
        wrong_answer2=round(self.q_variables[choose_train]/hours_traveled,2)
        wrong_answer3=round(hours_traveled*difference_speed,2)

        return (answer,wrong_answer1,wrong_answer2,wrong_answer3)



two_trains=TwoTrainsQuestion()
question_test=two_trains.generate_question() #Tuple with question and choices
                                            #First choice is answer other 3 are wrong answers
print(question_test)

NUM_Q_CREATE=10 #number of questions to generate

list_q_s=[]

for n in range(NUM_Q_CREATE):
    question=two_trains.generate_question()
    q_breakdown=[]
    q_breakdown.append(question[0])
    q_breakdown.append(question[1][0])
    q_breakdown.append(question[1][1])
    q_breakdown.append(question[1][2])
    q_breakdown.append(question[1][3])
    list_q_s.append(q_breakdown)

# Output the questions to csv
df=pd.DataFrame(list_q_s,columns=['question','answer','wrong_answer1','wrong_answer2', 'wrong_answer3'])
df.to_csv('10_questions_2_trains.csv')
