#Inus Grobler
#April 2020

import random
import pandas as pd

"""
Question: If + means divide, x means minus, + means multiply and - means plus, then find the value of 9 + 3 + 4 - 8 x 2 ?
"""

class SwapOperators():
    """
    The class object is used to create a multiple choice question of the form:
        Question: If + means divide, + means multiply and - means plus, then find the value of 9 + 3 + 4 - 8 x 2?

    Parameters
    ----------
    size: integer
        The size of the question i.e. the amount of digits added to the calculation
    """
    def __init__ (self, size=5):
        self.ORDER_OF_OPERATIONS=['/','*','+','-']
        self.SIZE=size
        self.OPERATOR_DICTIONARY={0:'/', 1:'*', 2:'+', 3:'-'}
        self.shuffled_operators={0:'/', 1:'*', 2:'+', 3:'-'}

    def calculation(self,nums,ops):
        """
        Calculates the answer to a list of number and a list of operators
        The function first calculates the divisions, then multiplication
        and lastly addition

        Parameters
        ----------
        nums: list of integers or floats
            A list of numbers that is used in the calculation

        ops: list of strings
            A list of strings with the string operators in the order they
            are written down in the question (i.e. not in order of calculation)

        Returns
        -------
        This function returns the answer of the calculation rounded to 2 decimal places
        """
        nums_calc,ops_calc=self.convert_to_negative(nums,ops)
        for oo in ['/','*','+']: #The order of calculations
            for k in range(len(nums_calc)):
                for index in range(len(ops_calc)):
                    #Division
                    if ops_calc[index] == '/' and oo == '/':
                        nums_calc[index]=nums_calc[index]/nums_calc[index+1]
                        nums_calc.pop(index+1)
                        ops_calc.pop(index)
                        break #break the loop once a calculation is made in order to go through the shorter list of numbers
                    #Multiplication
                    if ops_calc[index] == '*' and oo == '*':
                        nums_calc[index]=nums_calc[index]*nums_calc[index+1]
                        nums_calc.pop(index+1)
                        ops_calc.pop(index)
                        break #break the loop once a calculation is made in order to go through the shorter list of numbers
                    #Addition
                    if ops_calc[index] == '+' and oo == '+':
                        nums_calc[index]=nums_calc[index]+nums_calc[index+1]
                        nums_calc.pop(index+1)
                        ops_calc.pop(index)
                        break #break the loop once a calculation is made in order to go through the shorter list of numbers
        return round(nums_calc[0],2)

    def convert_to_negative(self,nums, ops):
        """
        This function changes the numbers with a negative operator
        before the number to a negative number and then changes the operator
        to addition. i.e. addition of a negative number.

        Parameters
        ----------
        nums: list of integers or floats
            A list of numbers that is used in the calculation

        ops: list of strings
            A list of strings with the string operators in the order they
            are written down in the question (i.e. not in order of calculation)

        Returns
        -------
        This function a tuple of two lists.
        The first list in the tuple is the new numbers with the negative values
        The second list in the tuple is the new operators with no minus
        """
        #Copy the variables in the memory in order not to change the original list
        nums_con_neg=nums.copy()
        ops_con_neg=ops.copy()
        for index in range(len(ops_con_neg)): #go through list of operators
            #if the sign is negative change numbers sign and change to addition
            if ops_con_neg[index]=='-':
                ops_con_neg[index]='+'
                nums_con_neg[index+1]=nums_con_neg[index+1]*-1
        return (nums_con_neg, ops_con_neg)

    def generate_numbers(self, start, end, size):
        """
        This funtion generates a list of random numbers with the specified
        start value, end value and the amount of numbers to generate.

        Parameters
        ----------
        start: interger
            The lowest value of the random numbers to be generated

        end: interger
            The highest value of the random numbers to be generated

        size: integer
            The amount of numbers to generate

        Returns
        -------
        A list of random numbers are returned
        """
        rand_num=[]
        for k in range(size):
            rand_num.append(random.randint(start,end))
        return rand_num

    def convert_to_operators(self,operator_nums, shuffled=False):
        """
        This funtion converts the operator values to the operator strings
        i.e. for 0 change to '/' and 1 change to '*' etc.

        Parameters
        ----------
        operator_nums: list of integers
            A list of the operator numbers

        shuffled: Boolean
            default value is False. If True the list of operators are also
            shuffled

        Returns
        -------
        A list of operator strings
        """
        #If shuffled True, shuffle the operators dictionary
        if shuffled:
            self.shuffled_operators=self.shuffle_dictionary(self.OPERATOR_DICTIONARY)
            operator_dictonary=self.shuffled_operators
        else:
            operator_dictonary=self.OPERATOR_DICTIONARY

        list_operators=[]
        for ops in operator_nums:
            list_operators.append(operator_dictonary[ops]) #get the string value of the operator using the key of the dictionary
        return list_operators

    def shuffle_dictionary(self,dictionary_to_shuffle):
        """
        This function shuffles a dictionary key and value pairs.

        Parameters
        ----------
        dictionary_to_shuffle: dictionary
            A dictionary that needs to be shuffled

        Returns
        -------
        A shuffled dictionary is returned
        """
        d=dictionary_to_shuffle.copy()
        for k in d.keys():
            j = random.randint(0,3)
            temp=d[j]
            d[j]=d[k]
            d[k]=temp
        return d

    def math_to_english(self, symbol):
        """
        This function converts a maths operator to a word
        such as '+' gets converted to plus.

        Parameters
        ----------
        symbol: string
            The symbol to convert to text is taken as parameter

        Returns
        -------
        A string with the word of the input symbol
        """
        if symbol=='/':
            return 'divide'
        elif symbol=='*':
            return 'multiply'
        elif symbol=='+':
            return 'plus'
        else:
            return 'minus'

    def math_to_character(self, symbol):
        """
        This function converts a maths operator to a more readable operator
        such as '/' gets converted to '÷'.

        Parameters
        ----------
        symbol: string
            The symbol to convert to a more readable character is taken as parameter

        Returns
        -------
        A string with the converted symbol
        """
        if symbol=='/':
            return '÷'
        elif symbol=='*':
            return 'x'
        elif symbol=='+':
            return '+'
        else:
            return '-'

    def generate_question(self):
        """
        This function generates a question and the respective choices for possible answers.

        The function makes sure that the choices are all different and that all the symbols have been swapped

        The numbers and oeprators are randomly generated to output a random question everytime

        Returns
        -------
        A tuple with the question and a list of the 4 possible choices
        """
        different_choices=True #Used to check if the 4 choices are different

        while different_choices: #Generate a question until the 4 choices are different
            #Generate the numbers and operators randomly
            self.numbers=self.generate_numbers(1,9,self.SIZE)
            self.operators_nums=self.generate_numbers(0,3,self.SIZE-1)
            #Convert the operators to symbols and shuffle the operators for the change of symbols in question
            self.operators=self.convert_to_operators(self.operators_nums)
            self.operators_swapped=self.convert_to_operators(self.operators_nums,True)
            #Get choices for the multiple choice question
            choices=self.create_choices() #Tuple of answer, wrong answer 1,2 and 3
            #validate if the choices and symbol swapping in the question are all different
            changed_symbols=0
            for k,v in self.OPERATOR_DICTIONARY.items():
                if v!=self.shuffled_operators[k]:
                    changed_symbols+=1
            if changed_symbols == 4 and len(set(choices))==4:
                different_choices=False
            #Creating question text
            question=''
            amount_character_changes=len(set(self.operators_nums)) #Used to check if last operator
            count=0
            for k,v in self.OPERATOR_DICTIONARY.items():
                if k in self.operators_nums:
                    count+=1
                    if count==1: #If it is first operator add "If"
                        question+=f"If {self.math_to_character(v)} means {self.math_to_english(self.shuffled_operators[k])}"
                    elif count==amount_character_changes: #If it is last operator add 'and'
                        question+=f" and {self.math_to_character(v)} means {self.math_to_english(self.shuffled_operators[k])}"
                    else:
                        question+=f", {self.math_to_character(v)} means {self.math_to_english(self.shuffled_operators[k])}"

            question+=f" then find the value of {str(self.numbers[0])} "

            for k,v in zip(range(len(self.operators)),self.operators):
                question+=f"{self.math_to_character(v)} {str(self.numbers[k+1])} "

            question+="?"

        return (question, choices)

    def create_choices(self):
        """
        This function generates the different choices for the multiple choice

        answer: the answer of the question with the operators swapped
        wrong_answer1: a wrong answer with no swapping of operators
        wrong_answer2: a wrong answer with the operators shuffled randomly to produce a wrong answer
        wrong_answer3: a wrong answer with the numbers shuffled randomly to produce a wrong answer

        Returns
        -------
        A tuple with the 4 possible choices in the multiple choice
        """
        answer=self.calculation(self.numbers, self.operators_swapped)

        wrong_answer1=self.calculation(self.numbers, self.operators)

        operators_random=random.sample(self.operators, len(self.operators))
        wrong_answer2=self.calculation(self.numbers, operators_random)

        numbers_random=random.sample(self.numbers, len(self.numbers))
        wrong_answer3=self.calculation(numbers_random, self.operators)

        return (answer, wrong_answer1, wrong_answer2, wrong_answer3)

swap_operators=SwapOperators()
question_test=swap_operators.generate_question() #Tuple with question and choices
                                            #First choice is answer other 3 are wrong answers

print(question_test)
NUM_Q_CREATE=10

list_q_s=[]

for n in range(NUM_Q_CREATE):
    question=swap_operators.generate_question()
    q_breakdown=[]
    q_breakdown.append(question[0])
    q_breakdown.append(question[1][0])
    q_breakdown.append(question[1][1])
    q_breakdown.append(question[1][2])
    q_breakdown.append(question[1][3])
    list_q_s.append(q_breakdown)

df=pd.DataFrame(list_q_s,columns=['question','answer','wrong_answer1','wrong_answer2', 'wrong_answer3'])
df.to_csv('10_questions_swapped_operator.csv')
